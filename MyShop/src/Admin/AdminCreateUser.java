package Admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author kklee998
 */
public class AdminCreateUser {

    private String userID;
    private String fname;
    private String lname;
    private String gender;
    private String phone;
    private String address;
    private String password;

    public String getFname(String fname) {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname(String lname) {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getGender(String gender) {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword(String password) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int n = 0; n < bytes.length; n++) {
                sb.append(Integer.toString((bytes[n] & 0xff) + 0x100, 16).substring(1));
            }

            password = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone(String phone) {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress(String address) {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserID(String x) {
        return x;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

}

class CreateManager extends AdminCreateUser {

    public String ManagerID() {
        File f = new File("user.txt");
        if (f.exists() != true) {
            try {
                PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
                writer.println("ID;First Name;Last Name;Gender;Contact;Privillege;Address;Password");
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
        }
        int i = -1;
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                String newline[] = line.split("(;|\n)");
                i += 1;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        String uniqID = ("M" + Integer.toString(i));
        return uniqID;
    }

    public boolean addNewManager(String Fname, String Lname, String Gender, String Phone, String Password, String Address) {

        File f = new File("user.txt");
        String userID = this.getUserID(ManagerID());
        String fname = this.getFname(Fname);
        String lname = this.getLname(Lname);
        String gender = this.getGender(Gender);
        String phone = this.getPhone(Phone);
        String password = this.getPassword(Password);
        String address = this.getAddress(Address);

        String line = "";
        line += userID + ";";
        line += fname + ";";
        line += lname + ";";
        line += gender + ";";
        line += phone + ";";
        line += "Manager" + ";";
        line += address + ";";
        line += password;

        System.out.println(line);

        try (PrintWriter writer = new PrintWriter(new FileOutputStream(f, true))) {
            writer.println(line);
            writer.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

}

class CreateCustomer extends AdminCreateUser {

    public String CustomerID() {
        File f = new File("user.txt");
        if (f.exists() != true) {
            try {
                PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
                writer.println("ID;First Name;Last Name;Gender;Contact;Privillege;Address;Password");
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
        }
        int i = -1;
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                String newline[] = line.split("(;|\n)");
                i += 1;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        String uniqID = ("C" + Integer.toString(i));
        return uniqID;
    }

    public boolean addNewCustomer(String Fname, String Lname, String Gender, String Phone, String Password, String Address) {

        File f = new File("user.txt");
        String userID = this.getUserID(CustomerID());
        String fname = this.getFname(Fname);
        String lname = this.getLname(Lname);
        String gender = this.getGender(Gender);
        String phone = this.getPhone(Phone);
        String password = this.getPassword(Password);
        String address = this.getAddress(Address);

        String line = "";
        line += userID + ";";
        line += fname + ";";
        line += lname + ";";
        line += gender + ";";
        line += phone + ";";
        line += "Customer" + ";";
        line += address + ";";
        line += password;

        System.out.println(line);

        try (PrintWriter writer = new PrintWriter(new FileOutputStream(f, true))) {
            writer.println(line);
            writer.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

}
