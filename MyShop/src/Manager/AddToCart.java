/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author kklee998
 */
public class AddToCart {

    public void AddCart(String itemID, String itemName, int quantity, int sum) {
        File f = new File("cart.tmp");
        Checkout checkout = new Checkout();
        f.deleteOnExit();
        try {
            String line = "";
            line += checkout.NewOrderID() + ";";
            line += itemID + ";";
            line += itemName + ";";
            line += Integer.toString(quantity) + ";";
            line += Integer.toString(sum);

            System.out.println(line);
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(f, true))) {
                writer.println(line);
                writer.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}


class CustomerAddToCart extends AddToCart {

}
