/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author kklee998
 */
public class Checkout {

    private String orderID;
    private String userID;
    private String productID;
    private String numItems;
    private String sumItems;

    public String getOrderID(String orderID) {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getUserID(String userID) {

        File f = new File("customer.tmp");
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                userID = line;
            }
        } catch (FileNotFoundException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        }
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getNumItems(String numItems) {
        File f = new File("cart.tmp");
        try {
            int k = 0;
            int i = 0;
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                String newline[] = line.split("(;|\n)");
                numItems = newline[3];
                i = Integer.valueOf(numItems);
                k += i;
                numItems = String.valueOf(k);

            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }

        return numItems;
    }

    public void setNumItems(String numItems) {
        this.numItems = numItems;
    }

    public String getSumItems(String sumItems) {
        File f = new File("cart.tmp");
        int i = 0;
        int k = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                String newline[] = line.split("(;|\n)");
                sumItems = newline[4];
                i = Integer.valueOf(sumItems);
                k += i;
                sumItems = String.valueOf(k);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        return sumItems;
    }

    public void setSumItems(String sumItems) {
        this.sumItems = sumItems;
    }

    public String NewOrderID() {
        File f = new File("order.txt");

        if (f.exists() != true) {
            try {
                PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
                writer.println("OrderID;CustomerID;Number of items;Total Order Amount;");
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
        }

        int i = -1;

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            while ((line = br.readLine()) != null) {
                String newline[] = line.split("(;|\n)");
                i += 1;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        String uniqID = ("Ord" + Integer.toString(i));
        return uniqID;

    }

    public boolean NewOrder() {
        File f = new File("order.txt");
        String orderID = this.getOrderID(NewOrderID());
        String userId = this.getUserID(userID);
        String qty = this.getNumItems(numItems);
        String sum = this.getSumItems(sumItems);

        String line = "";
        line += orderID + ";";
        line += userId + ";";
        line += qty + ";";
        line += sum;

        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
            writer.println(line);
            writer.close();

        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
        return true;
    }

    public void NewOrderItem() {
        File cartf = new File("cart.tmp");
        File f = new File("orderItem.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(cartf));
            String line;
            while ((line = br.readLine()) != null) {
                String writeLine = line;
                try {
                    PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
                    writer.println(writeLine);
                    writer.close();
                } catch (Exception e) {
                    System.err.println(e);
                }
            }

        } catch (Exception e) {
            System.err.println(e);
        }

    }
}
