/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 *
 * @author kklee998
 */
public abstract class Product {

    public String productID;
    public String productName;
    public double price;

    public Product(String productID, String productName, double price) {
        this.productID = productID;
        this.productName = productName;
        this.price = price;
    }

    public void checkFile() {

        File f = new File("product.txt");
        if (f.exists() != true) {
            try {
                PrintWriter writer = new PrintWriter(new FileOutputStream(f, true));
                writer.println("ID;Name;Price;Type");
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            }
        }
    }
}

class NonFragile extends Product {

    Integer NonfragileCost = 2;

    public NonFragile(String productID, String productName, Integer price) {
        super(productID, productName, price);
        super.checkFile();

        price = price + NonfragileCost;
        try {
            File f = new File("product.txt");
            String line = "";
            line += productID + ";";
            line += productName + ";";
            line += Integer.toString(price) + ";";
            line += "Non-Fragile";

            System.out.println(line);
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(f, true))) {
                writer.println(line);
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);

            }
        } catch (Exception e) {
            System.out.println(e);

        }

    }
}

class Fragile extends Product {

    Integer fragileCost = 5;

    public Fragile(String productID, String productName, Integer price) {
        super(productID, productName, price);
        super.checkFile();

        price = price + fragileCost;
        try {
            File f = new File("product.txt");
            String line = "";
            line += productID + ";";
            line += productName + ";";
            line += Integer.toString(price) + ";";
            line += "Fragile";

            System.out.println(line);
            try (PrintWriter writer = new PrintWriter(new FileOutputStream(f, true))) {
                writer.println(line);
                writer.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);

            }
        } catch (Exception e) {
            System.out.println(e);

        }

    }
}
