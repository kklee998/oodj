# README #

This README would normally document whatever steps are necessary to get your application up and running.

# TO-DO LIST #

### Login Access ###


User         | Function
-------      | --------
__Admin__    | Manage Account for Manager (Add/Delete/Edit/View/Search)
             | Manage Account for User    (Add/Delete/Edit/View/Search)
__Manager__  | Product
             | Order
__Customer__ | Search
		     | Add product to cart
		     | Display Bill 

### Order ###

orderID (PK)

customerID (FK)

productID (FK)

amountOfProduct

sumOfProduct

sumOfPrice

### Product ###

productID (PK)

productName

productDescription

productPrice

productType

### Customer ###

customerID

customerName

customerAddress

customerPhoneNumber


